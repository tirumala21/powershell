

param ( [Parameter(Mandatory=$false)] [String]$octopusURL,[Parameter(Mandatory=$false)] [String]$octopusAPIKey,
[Parameter(Mandatory=$false)] [String]$spaceName,[Parameter(Mandatory=$false)] [String]$role,[Parameter(Mandatory=$false)] [String]$projectName)


$ErrorActionPreference = "Stop";

# Define working variables
#$octopusURl="https://tirumala18.octopus.app/"
#$octopusAPIKey = "API-NJDQ6NI5HVUFRD11PMSLNW2YQ7IOVTS"
$header = @{ "X-Octopus-ApiKey" = $octopusAPIKey }
#$spaceName = "default"
#$role = "My role"
# Project details
#$projectName = "OctopusAutomation"


# Get space
$space = (Invoke-RestMethod -Method Get -Uri "$octopusURL/api/spaces/all" -Headers $header) | Where-Object {$_.Name -eq $spaceName}

# Get project
$project = (Invoke-RestMethod -Method Get -Uri "$octopusURL/api/$($space.Id)/projects/all" -Headers $header) | Where-Object {$_.Name -eq $projectName}

# Get deployment process
$deploymentProcess = (Invoke-RestMethod -Method Get -Uri "$octopusURL/api/$($space.Id)/deploymentprocesses/$($project.DeploymentProcessId)" -Headers $header)

# Get current steps
$steps = $deploymentProcess.Steps




$json = '{
      "Name": "Deploy Amazon ECS Service",
      "PackageRequirement": "AfterPackageAcquisition",
      "Properties": {
        "Octopus.Action.TargetRoles": $role
      },
      "Condition": "Success",
      "StartTrigger": "StartAfterPrevious",
      "Actions": [
        {
          "Name": "Deploy Amazon ECS Service",
          "ActionType": "aws-ecs",
          "Notes": null,
          "IsDisabled": false,
          "CanBeUsedForProjectVersioning": true,
          "IsRequired": false,
          "WorkerPoolId": null,
          "Container": {
            "Image": null,
            "FeedId": null
          },
          "WorkerPoolVariable": "",
          "Environments": [],
          "ExcludedEnvironments": [],
          "Channels": [],
          "TenantTags": [],
          "Packages": [
            {
              "Id": "1e5882a7-0e3d-481d-a7a0-c3be42d26481",
              "Name": "colortest",
              "PackageId": "colortest",
              "FeedId": "Feeds-1012",
              "AcquisitionLocation": "NotAcquired",
              "StepPackageInputsReferenceId": "649724d6-740c-49bf-98e5-b2bce441a6fa",
              "Properties": {}
            }
          ],
          "Condition": "Success",
          "Properties": {},
          "StepPackageVersion": "1.1.1",
          "LastSavedStepPackageVersion": "1.1.1",
          "Inputs": {
            "name": "colortestdef",
            "containers": [
              {
                "containerName": "colortest",
                "containerImageReference": {
                  "referenceId": "649724d6-740c-49bf-98e5-b2bce441a6fa",
                  "imageName": "colortest",
                  "feedId": "Feeds-1012"
                },
                "repositoryAuthentication": {
                  "type": "default"
                },
                "memoryLimitSoft": 250,
                "memoryLimitHard": 350,
                "containerPortMappings": [
                  {
                    "containerPort": 80,
                    "protocol": "tcp"
                  }
                ],
                "essential": true,
                "environmentFiles": [],
                "environmentVariables": [],
                "networkSettings": {
                  "disableNetworking": false,
                  "dnsServers": [],
                  "dnsSearchDomains": [],
                  "extraHosts": []
                },
                "containerStorage": {
                  "readOnlyRootFileSystem": false,
                  "mountPoints": [],
                  "volumeFrom": []
                },
                "containerLogging": {
                  "type": "auto"
                },
                "dockerLabels": [],
                "healthCheck": {
                  "command": [],
                  "interval": 30,
                  "timeout": 5
                },
                "dependencies": [],
                "stopTimeout": 2,
                "ulimits": []
              }
            ],
            "task": {
              "taskExecutionRole": "arn:aws:iam::841777056658:role/ecstaskexecutionrole",
              "cpu": 1024,
              "memory": 2048,
              "runtimePlatform": {
                "cpuArchitecture": "X86_64",
                "operatingSystemFamily": "LINUX"
              },
              "volumes": []
            },
            "networkConfiguration": {
              "securityGroupIds": [
                {
                  "id": "sg-0393fe57d863811ee"
                }
              ],
              "subnetIds": [
                {
                  "id": "subnet-092036d3e9fe53bfc"
                },
                {
                  "id": "subnet-024c48eda0abd6da5"
                },
                {
                  "id": "subnet-0ed0a7af2c54e462f"
                },
                {
                  "id": "subnet-0bfdc463cf8620713"
                },
                {
                  "id": "subnet-065666997a103353b"
                },
                {
                  "id": "subnet-08b1d934f59f093a6"
                }
              ],
              "autoAssignPublicIp": true
            },
            "desiredCount": 2,
            "additionalTags": {
              "enableEcsManagedTags": false,
              "tags": []
            },
            "minimumHealthPercent": 100,
            "maximumHealthPercent": 200,
            "waitOption": {
              "type": "waitUntilCompleted"
            },
            "loadBalancerMappings": [
              {
                "containerName": "colortest",
                "containerPort": 80,
                "targetGroupArn": "arn:aws:elasticloadbalancing:us-east-1:841777056658:targetgroup/octo/c204b6e203b43948"
              }
            ]
          },
          "AvailableStepPackageVersions": [
            "1.1.1"
          ],
          "Links": {}
        }
      ]
    }'
$jsonObj = $json | ConvertFrom-Json
$hash = @{}
foreach ($property in $jsonObj.PSObject.Properties) {
    $hash[$property.Name] = $property.Value
}





$steps += $hash


# Convert steps to json
$deploymentProcess.Steps = $steps
$jsonPayload = $deploymentProcess | ConvertTo-Json -Depth 10

# Submit request
Invoke-RestMethod -Method Put -Uri "$octopusURL/api/$($space.Id)/deploymentprocesses/$($project.DeploymentProcessId)" -Headers $header -Body $jsonPayload


